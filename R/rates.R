rates_2020 <- old_2020_rates <- list(
  fixed = 95,
  type = "Stepped",
  tiers = list(
    list(
      name = "Tier 1",
      gallon_min = 0,
      gallon_max = 12500,
      rate = 0
    ),
    list(
      name = "Tier 2",
      gallon_min = 12501,
      gallon_max = Inf,
      rate = 0.007
    )
  )
)

variable_rates <- list(
  fixed = 75,
  type = "Variable",
  tiers = list(
    list(
      name = "Tier 1",
      gallon_min = 0,
      gallon_max = 9999,
      rate = 0.002,
      fillcolor = "rgba(26, 152, 80, 0.6)",
      linecolor = "rgba(26, 152, 80, 1)"
    ),
    list(
      name = "Tier 2",
      gallon_min = 9999,
      gallon_max = 14999,
      rate = 0.003,
      fillcolor = "rgba(166, 217, 106, 0.6)",
      linecolor = "rgba(102, 189, 99, 1)"
    ),
    list(
      name = "Tier 3",
      gallon_min = 14999,
      gallon_max = 19999,
      rate = 0.004,
      fillcolor = "rgba(254, 224, 139, 0.6)",
      linecolor = "rgba(253, 174, 97, 1)"
    ),
    list(
      name = "Tier 4",
      gallon_min = 19999,
      gallon_max = 29999,
      rate = 0.006,
      fillcolor = "rgba(244, 109, 67, 0.6)",
      linecolor = "rgba(244, 109, 67, 1)"
    ),
    list(
      name = "Tier 5",
      gallon_min = 29999,
      gallon_max = 99999,
      rate = 0.013,
      fillcolor = "rgba(215, 48, 39, 0.6)",
      linecolor = "rgba(215, 48, 39, 1)"
    )
  )
)

stepped_rates <- variable_rates
stepped_rates$type <- "Stepped"

stepped_rates <- variable_rates
stepped_rates$type <- "Stepped"

stepped_rates10 <- stepped_rates
stepped_rates10$tiers <- lapply(stepped_rates10$tiers, FUN = function(tier) {
  tier$rate <- tier$rate * 1.1
  tier
})

jsk_rates <- stepped_rates
jsk_rates$tiers[[1]]$rate <- 0.002
jsk_rates$tiers[[2]]$rate <- 0.004
jsk_rates$tiers[[3]]$rate <- 0.007
jsk_rates$tiers[[4]]$rate <- 0.011
jsk_rates$tiers[[5]]$rate <- 0.016

october_rates <- list(
  fixed = 75,
  type = "Variable",
  tiers = list(
    list(
      name = "Tier 1",
      gallon_min = 0,
      gallon_max = 5000,
      rate = 0.0007,
      fillcolor = "rgba(26, 152, 80, 0.6)",
      linecolor = "rgba(26, 152, 80, 1)"
    ),
    list(
      name = "Tier 2",
      gallon_min = 5000,
      gallon_max = 10000,
      rate = 0.0019,
      fillcolor = "rgba(166, 217, 106, 0.6)",
      linecolor = "rgba(102, 189, 99, 1)"
    ),
    list(
      name = "Tier 3",
      gallon_min = 10000,
      gallon_max = 15000,
      rate = 0.0030,
      fillcolor = "rgba(254, 224, 139, 0.6)",
      linecolor = "rgba(253, 174, 97, 1)"
    ),
    list(
      name = "Tier 4",
      gallon_min = 15000,
      gallon_max = 20000,
      rate = 0.0040,
      fillcolor = "rgba(244, 109, 67, 0.6)",
      linecolor = "rgba(244, 109, 67, 1)"
    ),
    list(
      name = "Tier 5",
      gallon_min = 20000,
      gallon_max = 25000,
      rate = 0.0053,
      fillcolor = "rgba(215, 48, 39, 0.6)",
      linecolor = "rgba(215, 48, 39, 1)"
    ),
    list(
      name = "Tier 6",
      gallon_min = 25000,
      gallon_max = 99999,
      rate = 0.0063,
      fillcolor = "rgba(165, 15, 21, 0.6)",
      linecolor = "rgba(165, 15, 21, 1)"
    )
  )
)

rates_2021Q1 <- november_rates <- list(
  fixed = 80,
  type = "Variable",
  tiers = list(
    list(
      name = "Tier 1",
      gallon_min = 0,
      gallon_max = 5000,
      rate = 0.0007,
      fillcolor = "rgba(26, 152, 80, 0.6)",
      linecolor = "rgba(26, 152, 80, 1)"
    ),
    list(
      name = "Tier 2",
      gallon_min = 5000,
      gallon_max = 10000,
      rate = 0.0018,
      fillcolor = "rgba(166, 217, 106, 0.6)",
      linecolor = "rgba(102, 189, 99, 1)"
    ),
    list(
      name = "Tier 3",
      gallon_min = 10000,
      gallon_max = 15000,
      rate = 0.0029,
      fillcolor = "rgba(254, 224, 139, 0.6)",
      linecolor = "rgba(253, 174, 97, 1)"
    ),
    list(
      name = "Tier 4",
      gallon_min = 15000,
      gallon_max = 20000,
      rate = 0.0041,
      fillcolor = "rgba(244, 109, 67, 0.6)",
      linecolor = "rgba(244, 109, 67, 1)"
    ),
    list(
      name = "Tier 5",
      gallon_min = 20000,
      gallon_max = 25000,
      rate = 0.0052,
      fillcolor = "rgba(215, 48, 39, 0.6)",
      linecolor = "rgba(215, 48, 39, 1)"
    ),
    list(
      name = "Tier 6",
      gallon_min = 25000,
      gallon_max = 99999,
      rate = 0.0062,
      fillcolor = "rgba(165, 15, 21, 0.6)",
      linecolor = "rgba(165, 15, 21, 1)"
    )
  )
)

rates_2021 <- list(
  fixed = 50,
  type = "Stepped",
  tiers = list(
    list(
      name = "Tier 1",
      gallon_min = 0,
      gallon_max = Inf,
      rate = 0.00651
    )
  )
)
