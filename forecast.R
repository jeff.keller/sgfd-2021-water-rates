require(forecast)

source("R/system/meter_readings.R")
source("R/system/water_loss.R")

# https://otexts.com/fpp2/arima-r.html#modelling-procedure

account_usage <- processAccountData()
account_usage[, billingCycle := getBillingCycle(TDate)]
account_usage_quarterly <- account_usage[, .(Gallons = sum(Gallons)), by = billingCycle]
setorder(account_usage_quarterly, billingCycle)

account_usage_2021Q4 <- account_usage_quarterly[billingCycle < "2022-Q1"]
account_usage_2022Q1 <- account_usage_quarterly[billingCycle < "2022-Q2"]
account_usage_2022Q2 <- account_usage_quarterly[billingCycle < "2022-Q3"]
account_usage_2022Q3 <- account_usage_quarterly[billingCycle < "2022-Q4"]
account_usage_2022Q4 <- account_usage_quarterly[billingCycle < "2023-Q1"]
account_usage_2023Q4 <- account_usage_quarterly[billingCycle < "2024-Q1"]

# Forecast @ 2021-Q4 ---------------------------------------------------------------------------------------------------
account_usage_ts <- ts(account_usage_2021Q4$Gallons, start = c(2008, 4), frequency = 4)
plot(account_usage_ts, xlab = "Billing Cycle", ylab = "Gallons")

Acf(account_usage_ts)
Pacf(account_usage_ts)
fit <- auto.arima(account_usage_ts)

summary(fit)
Acf(fit$residuals)
Pacf(fit$residuals)

pred <- forecast(fit, h = 4)
summary(pred)
plot(pred)

revenue <- 190 * 50 + pred$mean * 0.00651
sum(revenue)

# Actual @ 2022-Q4
diff_quarterly <- pred$mean[-1] - account_usage_quarterly[billingCycle >= '2022-Q1', Gallons] # 138,000 gallons (2%)
diff_annual_revenue <- sum(diff_quarterly) * 0.00651 # ~$900

# 2022-Q4 revenue doesn't come in until 2023
# 2021-Q4 revenue is collected in 2022
gallons_2021Q4 <- account_usage_quarterly[billingCycle == '2021-Q4', Gallons]
gallons_2022Q4_pred <- pred$mean[4]
(gallons_2022Q4_pred - gallons_2021Q4) * 0.00651 # ~18,000 gallons / $119

# Forecast @ 2022-Q1 ---------------------------------------------------------------------------------------------------
account_usage_ts <- ts(account_usage_2022Q1$Gallons, start = c(2008, 4), frequency = 4)
plot(account_usage_ts, xlab = "Billing Cycle", ylab = "Gallons")

Acf(account_usage_ts)
Pacf(account_usage_ts)
fit <- auto.arima(account_usage_ts)

summary(fit)
Acf(fit$residuals)
Pacf(fit$residuals)

pred <- forecast(fit, h = 3)
summary(pred)
plot(pred)

revenue <- 190 * 50 + pred$mean * 0.00651
sum(revenue) + account_usage_quarterly[billingCycle == "2022-Q1", Gallons] * 0.00651 + 189 * 50

# 2022-Q1 expected: $21,237
# 2022-Q1 actual:   $20,188
# $1,049 less than expected for 2022-Q1
# Reasons:
#   - 150,000 less water used by customers
#   - Only collected 189 base rates (so $50 less)

# 2022-Q2 expected: $22,519.60
# 2022-Q2 actual:   $22,313.88

# 2022 total revenue forecast updated to $85,882
# Which is $1,427 less than expected ($87,308)

# Forecast @ 2022-Q4 ---------------------------------------------------------------------------------------------------
account_usage_ts <- ts(account_usage_2022Q3$Gallons, start = c(2008, 4), frequency = 4)
plot(account_usage_ts, xlab = "Billing Cycle", ylab = "Gallons")

Acf(account_usage_ts)
Pacf(account_usage_ts)
fit <- auto.arima(account_usage_ts)

summary(fit)
Acf(fit$residuals)
Pacf(fit$residuals)

pred <- forecast(fit, h = 4)
summary(pred)
plot(pred)

revenue <- 190 * 50 + pred$mean * 0.00651
sum(revenue) # $86,531 / 7,454,897 gallons (1.6% lower)

# Forecast @ 2023-Q1 ---------------------------------------------------------------------------------------------------
account_usage_ts <- ts(account_usage_2022Q4$Gallons, start = c(2008, 4), frequency = 4)
plot(account_usage_ts, xlab = "Billing Cycle", ylab = "Gallons")

Acf(account_usage_ts)
Pacf(account_usage_ts)
fit <- auto.arima(account_usage_ts)

summary(fit)
Acf(fit$residuals)
Pacf(fit$residuals)

pred <- forecast(fit, h = 3)
summary(pred)
plot(pred)

revenue2023_Q1_to_Q3 <- 190 * 50 + pred$mean * 0.00651
revenue2022_Q4 <- 190 * 50 + account_usage_2022Q4[billingCycle == "2022-Q4", Gallons] * 0.00651
revenue <- c(revenue2022_Q4, revenue2023_Q1_to_Q3) # 2022-Q4 revenue is realized in 2023 (and it is known by now)

sum(revenue) # $84,990 / 7,218,068 gallons (3.5% lower)

# Forecast @ 2023-Q4 ---------------------------------------------------------------------------------------------------
account_usage_ts <- ts(account_usage_2023Q4$Gallons, start = c(2008, 4), frequency = 4)
plot(account_usage_ts, xlab = "Billing Cycle", ylab = "Gallons")

Acf(account_usage_ts)
Pacf(account_usage_ts)
fit <- auto.arima(account_usage_ts)

summary(fit)
Acf(fit$residuals)
Pacf(fit$residuals)

pred <- forecast(fit, h = 4)
summary(pred)
plot(pred)

revenue <- 190 * 50 + pred$mean * 0.00651
sum(revenue) # $86,288 / 7,417,495 gallons (1.4% higher)
